import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';
import 'package:shake/shake.dart';

class Sardine extends StatelessWidget {
  const Sardine({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SMITrigger? _bump;

    void _hitBump() => _bump?.fire();

    void _onRiveInit(Artboard artboard) {
      final fishController =
          StateMachineController.fromArtboard(artboard, 'State');
      artboard.addController(fishController!);
      _bump = fishController.findInput<bool>('onTap') as SMITrigger;
      ShakeDetector.autoStart(onPhoneShake: () {
        _hitBump();
      });
    }

    return Center(
      child: SizedBox(
        height: 150,
        width: 150,
        child: GestureDetector(
          child: RiveAnimation.asset(
            'assets/animations/sardine.riv',
            fit: BoxFit.cover,
            onInit: _onRiveInit,
          ),
          onTap: _hitBump,
        ),
      ),
    );
  }
}

class SardineAnimation extends StatefulWidget {
  const SardineAnimation({Key? key}) : super(key: key);

  @override
  _SardineAnimationState createState() => _SardineAnimationState();
}

class _SardineAnimationState extends State<SardineAnimation>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Offset> _animation;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 20));
    _animation =
        Tween<Offset>(begin: const Offset(-1, 0.2), end: const Offset(1, 0.2))
            .animate(_animationController);

    _animationController.forward().whenComplete(() {
      _animationController.repeat();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SlideTransition(
      position: _animation,
      child: const Sardine(),
    ));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
