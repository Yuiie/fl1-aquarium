import 'package:aquarium/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:rive/rive.dart';

class ThemeSwitchStateMachine extends StatefulWidget {
  const ThemeSwitchStateMachine({Key? key}) : super(key: key);

  @override
  _ThemeSwitchStateMachineState createState() =>
      _ThemeSwitchStateMachineState();
}

class _ThemeSwitchStateMachineState extends State<ThemeSwitchStateMachine> {
  bool isDarkTheme = ThemeService().loadThemeFromBox();
  SMIInput<bool>? _setDarkTrigger;

  void _onStateChange(String stateMachineName, String stateName) {
    // State filter
    if (stateName == 'Off' && _setDarkTrigger?.value == true) {
      isDarkTheme = true;
      ThemeService().switchToDarkTheme();
    } else if (stateName == 'Idle_on' && _setDarkTrigger?.value == false) {
      isDarkTheme = false;
      ThemeService().switchToLightTheme();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Center(
        child: GestureDetector(
          onTapUp: (_) {
            var setDarkTrigger = _setDarkTrigger;
            if (setDarkTrigger != null) {
              setDarkTrigger.change(!setDarkTrigger.value);
            }
          },
          child: RiveAnimation.asset(
            isDarkTheme == false
                ? 'assets/animations/theme_switch_idle.riv'
                : 'assets/animations/theme_switch_off.riv',
            stateMachines: const ["State Machine 1"],
            onInit: (Artboard artboard) {
              setState(() {
                final controller = StateMachineController.fromArtboard(
                    artboard, 'State Machine 1',
                    onStateChange: _onStateChange);
                artboard.addController(controller!);
                _setDarkTrigger =
                    controller.findInput<bool>('on/off') as SMIBool;
                _setDarkTrigger!.change(isDarkTheme);
              });
            },
          ),
        ),
      ),
    );
  }
}
