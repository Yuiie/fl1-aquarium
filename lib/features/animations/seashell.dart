import 'package:flutter/material.dart';
import 'package:rive/rive.dart';
import 'package:shake/shake.dart';

class Seashell extends StatefulWidget {
  const Seashell({Key? key}) : super(key: key);

  @override
  _SeashellState createState() => _SeashellState();
}

class _SeashellState extends State<Seashell> {
  SMITrigger? _bump;

  void _onRiveInit(Artboard artboard) {
    final fishController =
        StateMachineController.fromArtboard(artboard, 'State');
    artboard.addController(fishController!);
    _bump = fishController.findInput<bool>('onTap') as SMITrigger;
    ShakeDetector.autoStart(onPhoneShake: () {
      _hitBump();
    });
  }

  void _hitBump() => _bump?.fire();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 150,
      width: 150,
      child: Center(
        child: GestureDetector(
          child: RiveAnimation.asset(
            'assets/animations/seashell.riv',
            fit: BoxFit.cover,
            onInit: _onRiveInit,
          ),
          onTap: _hitBump,
        ),
      ),
    );
  }
}
