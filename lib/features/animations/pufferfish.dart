import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:rive/rive.dart';
import 'package:shake/shake.dart';

class Pufferfish extends StatelessWidget {
  const Pufferfish({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SMITrigger? _bump;

    void _hitBump() => _bump?.fire();

    void _onRiveInit(Artboard artboard) {
      final fishController =
          StateMachineController.fromArtboard(artboard, 'State');
      artboard.addController(fishController!);
      _bump = fishController.findInput<bool>('onTap') as SMITrigger;
      ShakeDetector.autoStart(onPhoneShake: () {
        _hitBump();
      });
    }

    return Center(
      child: SizedBox(
        height: 150,
        width: 150,
        child: GestureDetector(
          child: RiveAnimation.asset(
            'assets/animations/pufferfish.riv',
            fit: BoxFit.cover,
            onInit: _onRiveInit,
          ),
          onTap: _hitBump,
        ),
      ),
    );
  }
}

class PufferfishAnimation extends StatefulWidget {
  const PufferfishAnimation({Key? key}) : super(key: key);

  @override
  _PufferfishAnimationState createState() => _PufferfishAnimationState();
}

class _PufferfishAnimationState extends State<PufferfishAnimation>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<Offset> _animation;

  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 30));
    _animation =
        Tween<Offset>(begin: const Offset(-1, -0.2), end: const Offset(1, -0.2))
            .animate(_animationController);

    _animationController.forward().whenComplete(() {
      _animationController.repeat();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: SlideTransition(
      position: _animation,
      child: const Pufferfish(),
    ));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
