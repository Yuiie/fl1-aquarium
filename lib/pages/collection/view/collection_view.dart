import 'dart:async';
import 'dart:ui';
import 'package:aquarium/pages/menu.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../model/collection_model.dart';
import '../request/collection_request.dart';
import 'package:get/get.dart';
import '../controller/collection_controller.dart';
import 'collection_list_view.dart';

class _Card extends StatelessWidget {
  final CollectionModel conversation;
  final BuildContext context;
  final double w;
  const _Card(
      {Key? key,
      required this.conversation,
      required this.context,
      required this.w})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: 0.5,
      child: Transform.translate(
        offset: const Offset(0, 1.0),
        child: Container(
          height: w / 2.3,
          width: w,
          padding: EdgeInsets.fromLTRB(w / 20, 0, w / 20, w / 20),
          child: InkWell(
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent,
            onTap: () {
              HapticFeedback.lightImpact();
            },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(.2),
                  borderRadius: const BorderRadius.all(Radius.circular(30)),
                  border: Border.all(
                      color: Colors.white.withOpacity(.1), width: 1)),
              child: Padding(
                padding: EdgeInsets.all(w / 50),
                child: Cards(
                  conversation: conversation,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class CollectionWidget extends StatelessWidget {
  CollectionWidget({Key? key}) : super(key: key);

  final String pufferfishUrl = "https://i.imgur.com/wAZmRs4.png";
  final String seaweedUrl = "https://i.imgur.com/9Vi5AaG.png";

  final CollectionController collectionController =
      Get.put(CollectionController());

  Future<void> _getData() async {
    collectionController.clearItem();
    addPublication("Pufferfish", '1', pufferfishUrl, false);
    addPublication("Seaweed", '2', seaweedUrl, false);
    await CollectionReq.fetchCollection();
  }

  static void addPublication(
      String user, String content, String urlImage, bool visible) {
    CollectionController collectionController = Get.put(CollectionController());
    CollectionModel conversationModel = CollectionModel();
    conversationModel.setName(user);
    conversationModel.setDescription(content);
    conversationModel.setUrlImage(urlImage);
    conversationModel.setVisibility(visible);
    collectionController.addItem(conversationModel);
  }

  void addItem(String itemName, String itemId, String url, bool visible) async {
    //addPublication(itemName, itemId, url, visible);
    CollectionReq.addItemCollection(itemId, itemName, url);
  }

  @override
  Widget build(BuildContext context) {
    _getData();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Collection"),
      ),
      drawer: const Menu(),
      body: Stack(
        children: [
          RefreshIndicator(
              child: ListView(
                physics: const BouncingScrollPhysics(
                  parent: AlwaysScrollableScrollPhysics(),
                ),
                children: [
                  const SizedBox(height: 10),
                  const SizedBox(
                    height: 10.0,
                  ),
                  Obx(
                    () => Column(
                      children: collectionController.conversations.value
                          .map(
                            (book) => _Card(
                                conversation: book,
                                context: context,
                                w: MediaQuery.of(context).size.width),
                          )
                          .toList(),
                    ),
                  )
                ],
              ),
              onRefresh: _getData),
        ],
      ),
    );
  }
}
