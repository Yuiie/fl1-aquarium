import 'dart:developer';

import '../model/collection_model.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import '../request/collection_request.dart';

class _Icon extends StatelessWidget {
  final IconData icon;
  final double w;
  const _Icon({Key? key, required this.icon, required this.w})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      color: Colors.white,
      size: w / 10,
    );
  }
}

class _Image extends StatelessWidget {
  final String urlImage;
  const _Image({Key? key, required this.urlImage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: urlImage,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
        ),
      ),
      placeholder: (context, url) => const CircularProgressIndicator(),
      errorWidget: (context, url, error) => const Icon(Icons.error),
    );
  }
}

class Cards extends StatelessWidget {
  final CollectionModel conversation;

  const Cards({Key? key, required this.conversation}) : super(key: key);

  bool isImage() {
    if (conversation.urlImage!.isEmpty) {
      return false;
    }
    if (conversation.urlImage!.length == 1) {
      if (conversation.urlImage == null) {
        return false;
      }
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    String? title = conversation.name;
    String? subtitle = conversation.description;
    String? imageUrl = conversation.urlImage;
    bool? active = conversation.visible;
    IconData icon = Icons.favorite;
    double _w = MediaQuery.of(context).size.width;
    log("name= " +
        subtitle! +
        "content= " +
        title! +
        "img= " +
        imageUrl.toString());
    return Opacity(
        opacity: conversation.visible! ? 1.0 : .4,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                height: _w / 3,
                width: _w / 3,
                decoration: BoxDecoration(
                    color: Colors.white.withOpacity(.2),
                    borderRadius: BorderRadius.circular(20)),
                child: isImage()
                    ? _Image(urlImage: conversation.urlImage!)
                    : _Icon(icon: icon, w: _w)),
            SizedBox(width: _w / 40),
            SizedBox(
              width: _w / 2.05,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    maxLines: 2,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: _w / 20,
                      fontWeight: FontWeight.bold,
                      letterSpacing: 1,
                      wordSpacing: 1,
                    ),
                  ),
                  Text(
                    subtitle,
                    maxLines: 1,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.white.withOpacity(1),
                      fontSize: _w / 25,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  active!
                      ? Container()
                      : ElevatedButton(
                          style: Theme.of(context).elevatedButtonTheme.style,
                          onPressed: () => CollectionReq.addItemCollection(
                              subtitle, title, imageUrl!),
                          child: const Text(
                            'Add fish',
                            maxLines: 1,
                            softWrap: true,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 15,
                            ),
                          ),
                        ),
                ],
              ),
            ),
          ],
        ));
  }
}
