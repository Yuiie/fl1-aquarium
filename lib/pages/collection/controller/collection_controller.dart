import '../model/collection_model.dart';
import 'package:get/get.dart';

class CollectionController extends GetxController with SingleGetTickerProviderMixin {


  var conversations = RxList<CollectionModel>().obs;

  addItem(CollectionModel collectionModel) => conversations.value.add(collectionModel);
  clearItem() => conversations.value.clear();
}