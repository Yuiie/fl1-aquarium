class CollectionModel {
  String? name;
  String? description;
  String? urlImage;
  bool? visible;

  CollectionModel({this.name, this.description, this.urlImage, this.visible});

  void addItems(
      String name, String description, String urlImage, bool visible) {
    this.name = name;
    this.description = description;
    this.urlImage = urlImage;
    this.visible = visible;
  }

  void setUrlImage(String s) {
    urlImage = s;
  }

  void setName(String s) {
    name = s;
  }

  void setDescription(String s) {
    description = s;
  }

  void setVisibility(bool s) {
    visible = s;
  }

  String? getUrlImage() {
    return urlImage;
  }

  String? getDescription() {
    return description;
  }

  String? getName() {
    return name;
  }
}
