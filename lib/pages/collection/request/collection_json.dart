class JsonCollection {
  final String? error;
  final String? code;
  final List? collection;

  JsonCollection._({this.error, this.code, this.collection});

  factory JsonCollection.fromJson(Map<String, dynamic> json) {
    return JsonCollection._(
        error: json['error'],
        code: json['code'],
        collection: json['collection']);
  }
}

class JsonCollectionList {
  String? uid;
  String? fishUid;
  String? fishName;
  String? fishUrl;

  JsonCollectionList(this.uid, this.fishUid, this.fishName, this.fishUrl);

  JsonCollectionList._({this.uid, this.fishUid, this.fishName, this.fishUrl});

  factory JsonCollectionList.fromJson(Map<String, dynamic> json) {
    return JsonCollectionList._(
      uid: json['uid'],
      fishUid: json['fishUid'],
      fishName: json['fishName'],
      fishUrl: json['fishUrl'],
    );
  }
}
