import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import '../view/collection_view.dart';
import '../controller/collection_controller.dart';
import 'package:get/get.dart';
import 'package:aquarium/services/firebase_service.dart';

class CollectionReq {
  static fetchCollection() async {
    CollectionController collectionController = Get.put(CollectionController());

    FirebaseService _firebaseService = Get.find();
    String? email = _firebaseService.user!.email;
    String uid = "null";

    if (email != null) {
      uid = email;
    }

    var collection = FirebaseFirestore.instance
        .collection('Collection')
        .where("user", isEqualTo: uid);
    var querySnapshot = await collection.get();

    for (var queryDocumentSnapshot in querySnapshot.docs) {
      Map<String, dynamic> data = queryDocumentSnapshot.data();

      int i = 0;
      for (var val in collectionController.conversations.value) {
        if (val.description == data['fishUid']) {
          CollectionWidget.addPublication(
              data['fishName'], data['fishUid'], data['fishUrl'], true);
          collectionController.conversations.value.removeAt(i);
        }
        i++;
      }
    }
  }

  static addItemCollection(
      String itemId, String itemName, String itemUrl) async {
    FirebaseService _firebaseService = Get.find();
    String? email = _firebaseService.user!.email;
    String uid = "null";

    if (email != null) {
      uid = email;
    }

    var collection = FirebaseFirestore.instance
        .collection('Collection')
        .where("user", isEqualTo: uid)
        .where("fishUid", isEqualTo: itemId);
    var querySnapshot = await collection.get();
    if (querySnapshot.size > 0) {
      return;
    }

    try {
      await FirebaseFirestore.instance.collection('Collection').add({
        'user': uid,
        'fishUid': itemId,
        'fishName': itemName,
        'fishUrl': itemUrl
      });
      await fetchCollection();
    } catch (error) {
      log(error.toString());
    }
  }
}
