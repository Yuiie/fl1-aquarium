import 'controller/user_controller.dart';
import 'package:aquarium/services/firebase_service.dart';
import 'package:aquarium/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginWidget extends StatelessWidget {
  LoginWidget({Key? key}) : super(key: key);

  final FirebaseService _firebaseService = Get.find();

  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final UserController userController = Get.put(UserController());
  void _handleLogin(BuildContext context) {
    _firebaseService.auth
        .signInWithEmailAndPassword(
            email: _email.value.text, password: _password.value.text)
        .then((value) => {
              Navigator.pushReplacementNamed(context, "/aquarium"),
              Get.snackbar('Success', 'Login in !'),
              userController.addItem(_email.value.text),
            })
        .catchError(
      (onError) {
        Get.snackbar('Error', onError.message);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign in"),
      ),
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 0)),
            const Center(
              child: Text("Sign in", style: TextStyle(fontSize: 40)),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 40, 30, 20),
              child: TextField(
                controller: _email,
                decoration: InputDecoration(
                  hintText: 'Email',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Themes.lightTeal),
                  ),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 20),
              child: TextField(
                controller: _password,
                decoration: InputDecoration(
                  hintText: 'Password',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Themes.lightTeal),
                  ),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
              ),
            ),
            ElevatedButton(
              onPressed: () => _handleLogin(context),
              child: Text("Sign in", style: Theme.of(context).textTheme.button),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
              child: ElevatedButton(
                onPressed: () => Navigator.pushNamed(context, "/signup"),
                child:
                    Text("Sign up", style: Theme.of(context).textTheme.button),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
