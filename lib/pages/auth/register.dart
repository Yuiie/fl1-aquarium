import 'package:aquarium/services/firebase_service.dart';
import 'package:aquarium/utils/theme.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterWidget extends StatelessWidget {
  RegisterWidget({Key? key}) : super(key: key);

  final FirebaseService _firebaseService = Get.find();

  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  void _handleCreateUser(BuildContext context) {
    CollectionReference reference =
        FirebaseFirestore.instance.collection("Users");
    Map<String, String> userdata = {"Email": _email.value.text};

    _firebaseService.auth
        .createUserWithEmailAndPassword(
            email: _email.value.text, password: _password.value.text)
        .then((value) {
          reference
              .doc(_firebaseService.user!.uid)
              .set(userdata)
              .then((value) => {
            Navigator.pushReplacementNamed(context, "/aquarium"),
            Get.snackbar('Success', 'Account created !'),
          });
    }).catchError(
      (onError) {
        Get.snackbar('Error', onError.message);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Sign up"),
      ),
      body: Center(
        child: Column(
          children: [
            const Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 0)),
            const Center(
              child: Text("Sign up", style: TextStyle(fontSize: 40)),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 40, 30, 20),
              child: TextField(
                controller: _email,
                decoration: InputDecoration(
                  hintText: 'Email',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Themes.lightTeal),
                  ),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
                cursorColor: Colors.white,
                style: const TextStyle(color: Colors.white),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 20),
              child: TextField(
                controller: _password,
                decoration: InputDecoration(
                  hintText: 'Password',
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10.0),
                    borderSide: const BorderSide(color: Themes.lightTeal),
                  ),
                  isDense: true,
                  contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
                obscureText: true,
                enableSuggestions: false,
                autocorrect: false,
                cursorColor: Colors.white,
              ),
            ),
            ElevatedButton(
              onPressed: () => _handleCreateUser(context),
              child: Text("Sign up", style: Theme.of(context).textTheme.button),
            ),
          ],
        ),
      ),
    );
  }
}
