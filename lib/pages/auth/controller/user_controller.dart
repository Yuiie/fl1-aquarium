import 'package:get/get.dart';

class UserController extends GetxController with SingleGetTickerProviderMixin {
  RxString userEmail = "no".obs;

  addItem(String val) => userEmail.value = val;
}
