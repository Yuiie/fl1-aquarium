import 'package:aquarium/features/animations/pufferfish.dart';
import 'package:aquarium/features/animations/sardine.dart';
import 'package:aquarium/features/animations/seashell.dart';
import 'package:aquarium/features/animations/seaweed.dart';
import 'package:aquarium/pages/menu.dart';
import 'package:flutter/material.dart';
import 'collection/request/collection_request.dart';
import 'collection/controller/collection_controller.dart';
import 'package:get/get.dart';

import 'collection/view/collection_view.dart';

class Aquarium extends StatefulWidget {
  const Aquarium({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<Aquarium> createState() => _AquariumState();
}

class _AquariumState extends State<Aquarium> {
  bool pufferfish = false;
  bool seaweed = false;
  bool fish = false;

  void _scanQRCode() {
    Navigator.pushNamed(context, "/qrcode");
  }

  final String pufferfishUrl =
      "https://previews.123rf.com/images/briang77/briang771512/briang77151201005/49668881-poissons-vecteur-de-bande-dessin%C3%A9e-ic%C3%B4ne.jpg";
  final String seaweedUrl =
      "https://i.pinimg.com/originals/a7/22/a1/a722a1fdddf674590c1d6eaf27703dae.jpg";

  void refreshFish() async {
    CollectionWidget.addPublication("Pufferfish", '1', pufferfishUrl, false);
    CollectionWidget.addPublication("Seaweed", '2', seaweedUrl, false);
    // addPublication("Fish", '3', fishUrl, false);
    await CollectionReq.fetchCollection();
    CollectionController collectionController = Get.put(CollectionController());

    for (var val in collectionController.conversations.value) {
      if (val.visible == true) {
        switch (val.description) {
          case "1":
            pufferfish = true;
            break;
          case "2":
            seaweed = true;
            break;
          case "3":
            fish = true;
            break;
          default:
            break;
        }
        setState(() {});
      }
    }
  }

  @override
  void initState() {
    super.initState();
    refreshFish();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget.title)),
      drawer: const Menu(),
      body: Stack(
        children: [
          // TO ADD QR CODE ETC...
          const SardineAnimation(),
          pufferfish ? const PufferfishAnimation() : Container(),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Row(
                children: [
                  seaweed ? const Seaweed() : Container(),
                  // TO ADD QR CODE ETC...
                  const Seashell(),
                ],
              )
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _scanQRCode,
        tooltip: 'Scan QR Code',
        child: const Icon(Icons.qr_code),
      ),
    );
  }
}
