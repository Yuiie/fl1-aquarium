import 'package:aquarium/features/animations/theme_switch.dart';
import 'package:aquarium/utils/theme.dart';
import 'package:flutter/material.dart';
import 'package:aquarium/services/firebase_service.dart';
import 'package:get/get.dart';

class Menu extends StatelessWidget {
  const Menu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FirebaseService firebaseService = Get.find();

    return DefaultTextStyle.merge(
      style: const TextStyle(fontSize: 25),
      child: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Themes.lightTeal,
              ),
              child: Text('Menu', style: TextStyle(fontSize: 40)),
            ),
            ListTile(
              title: const Text('My Aquarium', style: TextStyle(fontSize: 25)),
              onTap: () {
                Navigator.pushReplacementNamed(context, "/aquarium");
              },
            ),
            ListTile(
              title: const Text("My Profile", style: TextStyle(fontSize: 25)),
              onTap: () {
                Navigator.pushReplacementNamed(context, "/profile");
              },
            ),
            ListTile(
              title:
                  const Text("My Collection", style: TextStyle(fontSize: 25)),
              onTap: () {
                Navigator.pushReplacementNamed(context, "/collection");
              },
            ),
            ListTile(
              title: const Text("Sign out", style: TextStyle(fontSize: 25)),
              onTap: () {
                firebaseService.signout();
                Navigator.pushReplacementNamed(context, "/signin");
                Get.snackbar("Signed out", "See you next time!");
              },
            ),
            const SizedBox(
              height: 400,
              child: ThemeSwitchStateMachine(),
            ),
          ],
        ),
      ),
    );
  }
}
