import 'dart:io';

import 'package:aquarium/pages/menu.dart';
import 'package:aquarium/services/firebase_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

class ProfileWidget extends StatefulWidget {
  const ProfileWidget({Key? key}) : super(key: key);

  @override
  State<ProfileWidget> createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final FirebaseService _firebaseService = Get.find();

  final TextEditingController _password = TextEditingController();
  final TextEditingController _confirmPassword = TextEditingController();

  late Future<String> _userProfileURL;

  _handleChangePWD() {
    if (_password.value.text.isEmpty) {
      Get.snackbar('Error', "Enter a password!");
      return;
    }
    if (_password.value.text != _confirmPassword.value.text) {
      Get.snackbar('Error', "Passwords do not match!");
      return;
    }
    _firebaseService.user!
        .updatePassword(_password.value.text)
        .then((value) => Get.snackbar("Success", "Changed password !"))
        .catchError((onError) => {Get.snackbar('Error', onError.message)});
  }

  _handleUploadAvatar() async {
    showDialog<ImageSource>(
      context: context,
      builder: (context) =>
          AlertDialog(content: const Text("Choose image source"), actions: [
        TextButton(
          child: const Text("Camera"),
          onPressed: () => Navigator.pop(context, ImageSource.camera),
        ),
        TextButton(
          child: const Text("Gallery"),
          onPressed: () => Navigator.pop(context, ImageSource.gallery),
        ),
      ]),
    ).then((ImageSource? value) async {
      if (value != null) {
        ImagePicker picker = ImagePicker();
        XFile? image = await picker.pickImage(
            source: value,
            imageQuality: 50,
            preferredCameraDevice: CameraDevice.front);
        await _firebaseService.uploadUserPicture(File(image!.path));
        _userProfileURL = _firebaseService.getUserPicture();
        setState(() {});
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _userProfileURL = _firebaseService.getUserPicture();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Profile"),
        ),
        drawer: const Menu(),
        body: FutureBuilder<String>(
            future: _userProfileURL,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                snapshot.printError();
              }
              return Center(
                  child: Column(children: [
                const Padding(padding: EdgeInsets.fromLTRB(20, 40, 20, 0)),
                CircleAvatar(
                  backgroundImage: NetworkImage(snapshot.hasData
                      ? snapshot.requireData
                      : "https://picsum.photos/201"),
                  radius: 50,
                ),
                const Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 0)),
                ElevatedButton(
                  onPressed: () => _handleUploadAvatar(),
                  child: Text("Upload avatar",
                      style: Theme.of(context).textTheme.button),
                ),
                const Padding(padding: EdgeInsets.fromLTRB(10, 50, 10, 0)),
                const Center(
                  child:
                      Text("Change password", style: TextStyle(fontSize: 30)),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 20, 30, 20),
                  child: TextField(
                    controller: _password,
                    decoration: InputDecoration(
                      hintText: 'New Password',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(color: Colors.teal)),
                      isDense: true,
                      contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                    ),
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 20),
                  child: TextField(
                    controller: _confirmPassword,
                    decoration: InputDecoration(
                      hintText: 'Confirm New Password',
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          borderSide: const BorderSide(color: Colors.teal)),
                      isDense: true,
                      contentPadding: const EdgeInsets.fromLTRB(10, 20, 10, 10),
                    ),
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                  ),
                ),
                ElevatedButton(
                  onPressed: () => _handleChangePWD(),
                  child:
                      Text("Save", style: Theme.of(context).textTheme.button),
                )
              ]));
            }));
  }
}
