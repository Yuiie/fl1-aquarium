import 'package:aquarium/pages/aquarium.dart';
import 'package:aquarium/pages/auth/login.dart';
import 'package:aquarium/pages/profile.dart';
import 'package:aquarium/pages/auth/register.dart';
import 'package:aquarium/services/firebase_service.dart';
import 'package:aquarium/utils/theme.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:aquarium/pages/qrcode/qrcode_view.dart';
import 'package:aquarium/pages/collection/view/collection_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await GetStorage.init();
  await Get.putAsync(() => FirebaseService().init());
  runApp(const MyApp());
}

class MyApp extends GetMaterialApp {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final FirebaseService firebaseService = Get.find();

    return GetMaterialApp(
      title: 'Aquarium',
      debugShowCheckedModeBanner: false,
      theme: ThemeService().loadThemeFromBox() ? Themes.dark : Themes.light,
      routes: {
        "/signin": (context) => LoginWidget(),
        "/signup": (context) => RegisterWidget(),
        "/profile": (context) => const ProfileWidget(),
        "/collection": (context) => CollectionWidget(),
        "/qrcode": (context) => const Qrcode(),
        "/aquarium": (context) => const Aquarium(title: 'Aquarium'),
      },
      home: firebaseService.isLogin()
          ? const Aquarium(title: 'Aquarium')
          : LoginWidget(),
    );
  }
}
