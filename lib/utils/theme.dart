import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class Themes {
  static const textButtonStyle = TextStyle(fontSize: 20, color: Colors.white);
  static const lightTeal = Color(0xff00dac6);

  static final light = ThemeData(
    brightness: Brightness.light,
    colorScheme: const ColorScheme.light(),
    appBarTheme: const AppBarTheme(backgroundColor: lightTeal),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(lightTeal))),
    textTheme: const TextTheme(button: textButtonStyle),
    primaryColor: Colors.white,
    primarySwatch: Colors.blueGrey,
    fontFamily: 'GeosansLight',
  );

  static final dark = ThemeData(
    brightness: Brightness.dark,
    colorScheme: const ColorScheme.dark(),
    elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(lightTeal))),
    textTheme: const TextTheme(button: textButtonStyle),
    primaryColor: Colors.black,
    primarySwatch: Colors.blueGrey,
    fontFamily: 'GeosansLight',
  );
}

class ThemeService {
  final _appData = GetStorage();
  final _key = 'isDarkMode';

  bool loadThemeFromBox() => _appData.read(_key) ?? true;

  saveThemeToBox(bool isDarkMode) => _appData.write(_key, isDarkMode);

  void switchTheme() {
    Get.changeThemeMode(loadThemeFromBox() ? ThemeMode.light : ThemeMode.dark);
    saveThemeToBox(!loadThemeFromBox());
  }

  void switchToDarkTheme() {
    Get.changeTheme(Themes.dark);
    saveThemeToBox(true);
  }

  void switchToLightTheme() {
    Get.changeTheme(Themes.light);
    saveThemeToBox(false);
  }
}
