import 'dart:developer';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class FirebaseService extends GetxService {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore firestore = FirebaseFirestore.instance;
  final FirebaseStorage storage = FirebaseStorage.instance;

  // final Rxn<User> _firebaseUser = Rxn<User>(null);

  bool isLogin() {
    return _auth.currentUser != null;
  }

  User? get user => _auth.currentUser;

  FirebaseAuth get auth => _auth;

  /// Service initializer
  Future<FirebaseService> init() async {
    log('$runtimeType ready!');
    return this;
  }

  Future<String> getUserPicture() async {
    if (user == null) {
      return "https://picsum.photos/200";
    }
    DocumentSnapshot<Map<String, dynamic>> userCollect =
        await firestore.collection("Users").doc(auth.currentUser!.uid).get();
    return userCollect.data()!["profilePicture"] ?? "https://picsum.photos/200";
  }

  Future<void> uploadUserPicture(File image) async {
    if (auth.currentUser == null) {
      return Future.error("User not connected or null");
    }

    final imageUuid = const Uuid().v4();
    Reference ref = storage.ref('profiles/$imageUuid.png');
    await ref.putFile(image);
    await firestore
        .collection("Users")
        .doc(auth.currentUser!.uid)
        .update({'profilePicture': await ref.getDownloadURL()});
  }

  void signout() async {
    await _auth.signOut().then((value) => log("Logout!"));
  }
}
