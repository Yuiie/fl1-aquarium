# Aquarium

![GitHub repo size](https://img.shields.io/github/repo-size/adrien2431/fl1-aquarium)
![GitHub contributors](https://img.shields.io/github/contributors/adrien2431/fl1-aquarium)
![GitHub stars](https://img.shields.io/github/stars/adrien2431/fl1-aquarium?style=social)
![GitHub forks](https://img.shields.io/github/forks/adrien2431/fl1-aquarium?style=social)

Aquarium is a relaxing application that allows any user to collect and view fishes they can interact with.

Just scan the QR codes of the fishes you want, and they'll be added to your collection. The codes can be found right below.

## Add Fishes

To add a fish to the aquarium, you can just click on the button Add Fish in the Collection menu,
or use a QR code :
- https://i.imgur.com/bK1Be45.png
- https://i.imgur.com/1wGP6l0.png

## Prerequisites

Before you begin, ensure you have met the following requirements:
* You have installed the 2.5.0 version (stable channel) of Flutter.
* You have a Windows/Linux machine (untested on MacOS).
* You have read and followed the steps indicated by the [Flutter docs](https://flutter.dev/docs/get-started/install), and have a clean `flutter doctor`.

## Installing Aquarium

To install Aquarium, follow these steps:

```
git clone https://github.com/adrien2431/fl1-aquarium.git
cd <path_to_Aquarium>
flutter pub get
```

## Using Aquarium

To use Aquarium, follow these steps:

```
cd <path_to_Aquarium>
flutter run
```

## Contributing to Aquarium
To contribute to Aquarium, follow these steps:

1. Fork this repository.
2. Create a branch: `git checkout -b <branch_name>`.
3. Make your changes and commit them: `git commit -m '<commit_message>'`
4. Push to the original branch: `git push origin <project_name>/<location>`
5. Create the pull request.

Alternatively see the GitHub documentation on [creating a pull request](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## Contributors

Thanks to the following people who have contributed to this project:

* [@Tiffaa](https://github.com/TiffaaMC)
* [@adrien2431](https://github.com/adrien2431)
* [@Yuiie](https://github.com/Yuiie)

## Contact

If you want to contact us you can reach us on Discord: `adrien2431#2431`.

## License

This project uses the following license: [CC BY-NC 3.0](https://creativecommons.org/licenses/by-nc/3.0/legalcode).
